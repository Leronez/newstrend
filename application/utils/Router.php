<?php

class Router
{
    private $routes;

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Removes all unnecessary in the path to the application folder
     * @param string $uri Original URI
     * @return string Cleaned URI
     */
    private function cleanURI(string $uri) {
        $uriItems = explode('/', $uri);

        for ($index = count($uriItems) - 1; $index >= 0; $index--) {
            if ($uriItems[$index] == 'application') {
                $uriItems = array_splice($uriItems, $index);
                break;
            }
        }

        return implode(',', $uriItems);
    }

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $routesPath = ROOT.'/configurations/routes.php';
        $this->routes = include ($routesPath);
    }

    public function run()
    {
        $cleanURI = $this->cleanURI($this->getURI());

        foreach ($this->routes as  $uriPattern => $path ) {
            if (preg_match("~$uriPattern~", $cleanURI)) {

                $internalRoute = preg_replace("~$uriPattern~", $path, $cleanURI);

                $segments = explode('/', $internalRoute);

                array_shift($segments);

                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action'.ucfirst(array_shift($segments));

                $parameters = $segments;

                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                $controllerObject = new $controllerName();
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if ($result != null) {
                    break;
                }
            }
            
        }
    }

}