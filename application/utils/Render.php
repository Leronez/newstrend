<?php

class Render
{

    public static function View($viewName, $data = null)
    {
        return require_once(ROOT. '/views/' . $viewName);
    }
}