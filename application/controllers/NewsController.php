<?php

include_once ROOT . '/models/News.php';


class NewsController
{
    public function actionAllNews()
    {
        $newsList = array();
        $newsList = News::getNewsList();

        Render::View('NewsList.php', $newsList);

        return true;
    }

    public function actionViewNews($newsId)
    {
        $news = News::getNewsItemById($newsId);

        Render::View('NewsItem.php', $news);

        return true;
    }
}