<?php
return array(

    'news/([0-9]+)' => 'news/viewNews/$1',
    'news' => 'news/AllNews',

    'index.php' => 'news/AllNews',

    'application' => 'application/news/AllNews',
);