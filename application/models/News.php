<?php

include_once ROOT . '/utils/Database.php';

class News
{
    public static function getNewsItemById($newsId)
    {
        $db = DataBase::getConnection();

        $result = $db->query('SELECT * FROM `news` WHERE `id` =' . $newsId);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $newsItem = $result->fetch();

        return $newsItem;
    }

    public static function getNewsList()
    {
        $db = DataBase::getConnection();

        $result = $db->query('SELECT `id`, `caption`, `body`, `createDate`, `updateDate`, `authorId` 
                    FROM `news`
                    ORDER BY `updateDate` DESC 
                    LIMIT 10');

        $index = 0;
        $newsList = array();

        while ($row = $result->fetch()) {
            $newsList[$index]['id'] = $row['id'];
            $newsList[$index]['caption'] = $row['caption'];
            $newsList[$index]['body'] = $row['body'];
            $newsList[$index]['createDate'] = $row['createDate'];
            $newsList[$index]['updateDate'] = $row['updateDate'];
            $newsList[$index]['authorId'] = $row['authorId'];

            $index++;
        }

        return $newsList;
    }

}