<?php

define('ROOT', dirname(__FILE__));

require_once(ROOT.'/utils/Router.php');
require_once(ROOT . '/utils/Render.php');

$router = new Router();
$router->run();